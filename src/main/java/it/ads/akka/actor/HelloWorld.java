package it.ads.akka.actor;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;

public class HelloWorld extends UntypedActor {

	@Override
	public void preStart() {
		/**
		 * Create the greeter actor
		 */
		final ActorRef greeter = getContext().actorOf(Props.create(Greeter.class), "greeter");
		/**
		 * Tell him to perfom the greeting
		 */
		greeter.tell(Greeter.Message.GREET, getSelf());
	}
	
	@Override
	public void onReceive(Object message) throws Throwable {
		if(message == Greeter.Message.DONE) {
			System.out.println("Terminating...");
			getContext().stop(getSelf());
		} else {
			System.out.println(this.getClass().getSimpleName() + " - onReceive - NOTIFY - msg: message received is not a Message.GREET type, message = [" + message + "]");
		}
		
	}

}
