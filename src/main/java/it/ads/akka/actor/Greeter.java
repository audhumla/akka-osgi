package it.ads.akka.actor;

import akka.actor.UntypedActor;

public class Greeter extends UntypedActor {
	
	public static enum Message {
		GREET, DONE;
	}

	@Override
	public void onReceive(Object message) throws Throwable {
		if(message == Message.GREET) {
			System.out.println("Hello World!");
			getSender().tell(Message.DONE,getSelf());
		} else {
			System.out.println(this.getClass().getSimpleName() + " - onReceive - NOTIFY - msg: message received is not a Message.DONE type, message = [" + message + "]");
			unhandled(message);
		}
			
	}

}
