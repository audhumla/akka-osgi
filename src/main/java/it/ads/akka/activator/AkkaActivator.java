package it.ads.akka.activator;

import org.osgi.framework.BundleContext;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.osgi.ActorSystemActivator;
import it.ads.akka.actor.HelloWorld;
import it.ads.akka.actor.Terminator; 

public class AkkaActivator extends ActorSystemActivator {

	ActorSystem actorSystem; 
	ActorRef actorRef;
	
	@Override
	public void configure(BundleContext context, ActorSystem system) {
		Config regularConfig = ConfigFactory.load();
		String sysName = system.name();
		ActorSystem.apply(sysName, regularConfig);
		actorRef = system.actorOf(Props.create(HelloWorld.class), "helloWorld");
		system.actorOf(Props.create(Terminator.class, actorRef), "terminator");
	}

}
