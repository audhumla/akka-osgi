# README #

### Build the project and start it in Karaf ###
* Open the command line in the sources directory
* Type `mvn clean install`

* Start Karaf and type in the Karaf command line:

```
#!

install mvn:com.example/akka-bundle/0.0.1
```